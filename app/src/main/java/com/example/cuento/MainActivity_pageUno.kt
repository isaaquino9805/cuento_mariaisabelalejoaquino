package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

const val EXTRA_MESSAGE2 = "com.example.myfirstapp.MESSAGE" //Agregar
class MainActivity_pageUno : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_uno)

        // Get the Intent that started this activity and extract the string
        val texto = intent.getStringExtra(EXTRA_MESSAGE)

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.textpage1).apply {
            text = texto
        }

    }
    fun sendMessage(view: View) {
        val texto2 ="Como todas las mañanas, Nicolás esta dormido, acurrucado en la torre de un misterioso castillo."
        val intent2 = Intent(this, MainActivity_pageDos::class.java).apply {
            putExtra(EXTRA_MESSAGE2, texto2)
        }
        startActivity(intent2)

    }
}