package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

const val EXTRA_MESSAGE4 = "com.example.myfirstapp.MESSAGE" //Agregar
class MainActivity_pageTres : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_tres)

        // Get the Intent that started this activity and extract the string
        val texto3 = intent.getStringExtra(EXTRA_MESSAGE3)

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.textpage3).apply {
            text = texto3
        }
    }
    fun sendMessage(view: View) {
        val texto4 ="¡ Qué travieso es Nicolás !"
        val intent4 = Intent(this, page_cuatro::class.java).apply {
            putExtra(EXTRA_MESSAGE4, texto4)
        }
        startActivity(intent4)

    }
}