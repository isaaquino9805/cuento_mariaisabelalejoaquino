package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_inicio.*


const val EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE" //Agregar
class MainActivity_Title : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.title)



    }
    //MÉTODO DE BOTON
    fun sendMessage(view: View) {
        val texto ="Nicolás es un fantasma, pequeñito y gordinflón. Le gusta comer galletas y beber en biberón"
        val intent = Intent(this, MainActivity_pageUno::class.java).apply {
            putExtra(EXTRA_MESSAGE, texto)
        }
        startActivity(intent)

    }
}