package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

const val EXTRA_MESSAGE3 = "com.example.myfirstapp.MESSAGE" //Agregar
class MainActivity_pageDos : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_dos)

        // Get the Intent that started this activity and extract the string
        val texto2 = intent.getStringExtra(EXTRA_MESSAGE2)

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.textpage2).apply {
            text = texto2
        }
    }
    fun sendMessage(view: View) {
        val texto3 ="Cuando vuelve a ser de noche, Nicolás sale a pasear diciendo \"¡ uh, uh, uh !\" y asustando por detrás."
        val intent3 = Intent(this, MainActivity_pageTres::class.java).apply {
            putExtra(EXTRA_MESSAGE3, texto3)
        }
        startActivity(intent3)

    }
}