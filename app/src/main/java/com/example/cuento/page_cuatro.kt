package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class page_cuatro : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_cuatro)

        // Get the Intent that started this activity and extract the string
        val texto4 = intent.getStringExtra(EXTRA_MESSAGE4)

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.textpage4).apply {
            text = texto4
        }
    }
}